import docker
import os

# Create a Docker client object
client = docker.from_env()

list = ["020", "021", "022", "024", "025", "027", "028", "029", "030", "031"]

for container in list:
    logs = client.containers.get(container).logs(stream=False, tail=5)
    decoded_logs = logs.decode("utf-8")
    count = decoded_logs.count("Version is up to date")

    if count > 4:
        print(f"{container} - ERROR ({count})")
        os.system(f"docker restart {container}")
    else:
        print(f"{container} - OK ({count})")